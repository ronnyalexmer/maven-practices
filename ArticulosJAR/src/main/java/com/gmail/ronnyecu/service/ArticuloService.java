package com.gmail.ronnyecu.service;

import com.gmail.ronnyecu.model.Articulo;

import java.util.ArrayList;
import java.util.List;

public class ArticuloService {
    public List<Articulo> getArticulos() {
        List<Articulo> lista = new ArrayList<Articulo>();
        Articulo a1 = new Articulo();
        lista.add(new Articulo(1, "PA", "Pantalon", 35));
        lista.add(new Articulo(2, "CA", "Camisa", 55));
        lista.add(new Articulo(3, "ZA", "Zapatos", 25));


        return lista;
    }
}
