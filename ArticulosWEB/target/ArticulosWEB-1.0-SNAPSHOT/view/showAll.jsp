<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE HTML>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title of the document</title>
</head>

<body>
The content of the document......
</body>

<table border="1">
    <thead>
    <tr>
        <th>Id</th>
        <th>Codigo</th>
        <th>Nombre</th>
        <th>Precio</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="articulo" items="${articulos}">
        <tr>
            <td>${articulo.id}</td>
            <td>${articulo.codigo}</td>
            <td>${articulo.nombre}</td>
            <td>${articulo.precio}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>

</html>