package com.gmail.ronnyecu.controller;

import com.gmail.ronnyecu.service.ArticuloService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/articuloController")
public class ArticuloController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ArticuloController() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = (String) req.getParameter("action");
        switch (action) {
            case "showAll":
                showAll(req, resp);
                break;
            default:
                req.setAttribute("page", "index");
                req.getRequestDispatcher("index.jsp").forward(req, resp);
        }
        new ArticuloService().getArticulos().forEach(articulo -> System.out.println(articulo.toString()));
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }

    private void showAll(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("articulos", new ArticuloService().getArticulos());
        req.setAttribute("page", "showAll");
        req.getRequestDispatcher("view/showAll.jsp").forward(req, resp);
    }
}
